﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCRT.CopyFile
{
    class Start
    {
        string _adress;
        string _adressCopy;
        string _name;
        int _buffer;

        private WorkWithFile f;

        public Start(string adress, string adressCopy, string name, int buffer)
        {
             _adress = adress;
             _adressCopy = adressCopy;
             _name = name;
             _buffer = buffer;
        }

        public void StartWork()
        {
            var task = Task.Run(() =>
                {
                    //поток для чтения файла                  
                    FileStream input = File.Open(@"" + _adress, FileMode.Open);

                    // поток для записи                  
                    FileStream output = File.Open(@"" + _adressCopy + '/' + _name, FileMode.Create);

                    //размер буфера
                    int lengthBuffer = _buffer;

                    //количесвто потоков, можно поставить и 10 и 20 и 100500
                    int maxCap = 1;

                    f = new WorkWithFile(lengthBuffer, input, output, maxCap);
                }
            );
        }

        public void WriteStope()  // остановть запись
        {
            var taskWriteStop = Task.Run(() =>
            {
                f.WriteStop();
            });
        }

        public void WriteFile() // запустить запись
        {
            var taskWrite = Task.Run(() =>
          {
              f.Write();
          });
        }

        public void RadFail() // запустить чтение
        {
            var taskRead = Task.Run(() =>
            {                
                f.Read();
                return true;
            });
        }

        public void ReadStop() // остановить чтение
        {
            var taskRedStop = Task.Run(() =>
            {
                f.ReadStop();
            });
        }        
    }
}
