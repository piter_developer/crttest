﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace TestCRT
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();   
            blockBox.PreviewTextInput += new TextCompositionEventHandler(blockBox_PreviewTextInput);
        }        

        private void OpenDialogBTN_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if(openFileDialog.ShowDialog() == true)
            {
                FileInfo adress = new FileInfo(openFileDialog.FileName);
                adressBox.Text = adress.ToString();                
            }
        }
    
        private  void CopyBTN_Click(object sender, RoutedEventArgs e)
        {
            string adressFile = adressBox.Text;
            string adressCopyFile= adressCopyBox.Text;            
            string nameCopyFail;
            int buffer = Convert.ToInt32(blockBox.Text);

            if (newNameFailBox.Text == "") // если не дать имя копии то присвоится просто CopyFail
            {
                nameCopyFail = "CopyFail";
            }
            else nameCopyFail = newNameFailBox.Text;


            this.Visibility = Visibility.Collapsed; 

            Process process = new Process(adressFile, adressCopyFile, nameCopyFail, buffer);
            process.ShowDialog();

            this.Close();        
        }

        void blockBox_PreviewTextInput(object sender, TextCompositionEventArgs e) // позволяю пользоваетлю записать тольок цифры в строку указывающую размер буфера 
        {
            if (!Char.IsDigit(e.Text, 0)) e.Handled = true;
        }

        private void ExitBTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }      
    }
}
