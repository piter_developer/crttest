﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace TestCRT.CopyFile
{
    class WorkWithFile
    {
        //очередь для хранения буфера 	
        private ConcurrentQueue<byte[]> queue = new ConcurrentQueue<byte[]>();
        private int _maxCap;
        private readonly Stream _input;
        private readonly Stream _output;
        private byte[] _buffer;
        private CancellationTokenSource _cancellationTokenRead;
        private CancellationTokenSource _cancellationTokenWrite;
        private bool _isEndRead;
        //размер буффера 
        int _lengthBuffer;

        public WorkWithFile(int lengthBuffer, Stream input, Stream output, int maxCap)
        {
            _lengthBuffer = lengthBuffer;
            _input = input;
            _output = output;
            _maxCap = maxCap;
            _isEndRead = false;
        }

        public void Stop()
        {
            _cancellationTokenRead.Cancel();
            _cancellationTokenWrite.Cancel();
        }

        public void ReadStop()
        {
            _cancellationTokenRead.Cancel();
        }

        public void WriteStop()
        {
            _cancellationTokenWrite.Cancel();
        }     

        public async void Read() //пишем в буфер
        {
            _cancellationTokenRead = new CancellationTokenSource();
            while (_input.Length != _input.Position && !_cancellationTokenRead.IsCancellationRequested)
            {
                int count = 0;
                try
                {
                    _buffer = new byte[_lengthBuffer];
                    count = _input.Read(_buffer, 0, _lengthBuffer);

                    await Task.Delay(50);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Reading stop");
                    return;
                }

                while (queue.Count >= _maxCap && !_cancellationTokenRead.IsCancellationRequested)
                {
                    try
                    {
                       MessageBox.Show("Wait");
                       await Task.Delay(50, _cancellationTokenRead.Token);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Reading stop");
                        return;
                    }
                }

                if (count >= _lengthBuffer)
                {
                    queue.Enqueue(_buffer);
                }
                else
                {
                    byte[] bytes = new byte[count];
                    Array.Copy(_buffer, bytes, count);
                    queue.Enqueue(bytes);
                    MessageBox.Show("Копирование завершено");
                    _isEndRead = true;
                }
            }
        }

        public async void Write() // пишем из буфера
        {
            _cancellationTokenWrite = new CancellationTokenSource();
            while ((queue.Count != 0 || !_isEndRead) && !_cancellationTokenWrite.IsCancellationRequested)
            {
                byte[] data;
                if (queue.TryDequeue(out data))
                {
                    try
                    {
                        await _output.WriteAsync(data, 0, data.Length, _cancellationTokenWrite.Token);                
                    }
                    catch (Exception e)
                    {                      
                        MessageBox.Show("Writing stop");
                        return;
                    }
                }
            }
        }
    }
}
    