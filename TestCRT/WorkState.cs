﻿namespace TestCRT
{
    enum WorkState
    {
        Read = 1,
        StopRead = 2,
        Write = 3,
        StopWrite = 4
    }
}
