﻿using System.ComponentModel;
using System.Windows;
using TestCRT.CopyFile;

namespace TestCRT
{
    /// <summary>
    /// Логика взаимодействия для Process.xaml
    /// </summary>
    public partial class Process : Window, INotifyPropertyChanged
    {
        string _adress;
        string _adressCopy;
        string _name;
        int _buffer;

        private Start _start;

        private WorkState workStateRead = WorkState.StopRead;
        private WorkState workStateWrite = WorkState.StopWrite;

        internal WorkState WorkStateRead 
        {
            get { return workStateRead; }
            set
            {
                workStateRead = value;
                FirePropertyChanged("ReadOrStopReadBtn");
            }
        }

        public string ReadOrStopReadBtn // меняем текст в кнопке на запуск и остновку чтения в файл
        {
            get
            {
                return (WorkStateRead == WorkState.Read) ? "Stop Read" : "Rad";
            }
        }


        internal WorkState WorkStateWrite
        {
            get { return workStateWrite; }
            set
            {
                workStateWrite = value;
                FirePropertyChanged("WriteOrStopWtiteBtn");
            }
        }

        public string WriteOrStopWtiteBtn // меняем текст в кнопке на запуск и остновку записи в файл
        {
            get
            {
                return (WorkStateWrite == WorkState.Write) ? "Stop Write" : "Write";
            }
        }


        public Process(string adress, string adressCopy, string name, int buffer)
        {
            InitializeComponent();

            DataContext = this;

            _adress = adress;
            _adressCopy = adressCopy;
            _name = name;
            _buffer = buffer; 

            _start = new Start(adress, adressCopy, name, buffer);
            _start.StartWork();
        }      

        private void controlReadBTN_Click(object sender, RoutedEventArgs e)
        {                
                if (WorkStateRead == WorkState.Read)
                {
                    StopRead();
                }
                else if (WorkStateRead == WorkState.StopRead)
                {
                    FaileRead();
                }               
        }

        private void controlWriteBTN_Click(object sender, RoutedEventArgs e)
        {
          if(WorkStateWrite == WorkState.Write)
            {
                StopWrite();
            }
          else if(WorkStateWrite == WorkState.StopWrite)
            {
                FaileWrite();
            }
        }

        private void exitBTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        void StopRead()
        {
            _start.ReadStop();
            WorkStateRead = WorkState.StopRead;
        }

        void FaileRead()
        {
            _start.RadFail();
            WorkStateRead = WorkState.Read;
        }

        void StopWrite()
        {
            _start.WriteStope();
            WorkStateWrite = WorkState.StopWrite;
        }

        void FaileWrite()
        {
            _start.WriteFile();
            WorkStateWrite = WorkState.Write;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        #region INotifyPropertyChanged Members
        void FirePropertyChanged(string name)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}


